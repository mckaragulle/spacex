<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CapsulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('capsules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('capsule_serial', 20)->nullable();
            $table->string('capsule_id', 20)->nullable();
            $table->string('status', 20)->nullable();
            $table->dateTime('original_launch')->nullable();
            $table->integer('original_launch_unix')->nullable();
            $table->integer('landings')->nullable();
            $table->string('type')->nullable();
            $table->text('details')->nullable();
            $table->integer('reuse_count')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('capsules');
    }
}
