<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('missions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('capsule_id');
            $table->foreign('capsule_id')->references('id')->on('capsules')->onUpdate('cascade')->onDelete('cascade');
            $table->string('name', 191)->nullable();
            $table->integer('flight')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('missions');
        Schema::enableForeignKeyConstraints();
    }
}
