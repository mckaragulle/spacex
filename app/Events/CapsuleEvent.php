<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CapsuleEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * The capsule variable.
     *
     */
    public $capsule, $mission, $data;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(array $capsule)
    {
        $this->capsule = $capsule;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
