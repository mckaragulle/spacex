<?php

namespace App\Listeners;

use App\Events\CapsuleEvent;
use App\Models\Capsule;
use App\Models\Mission;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class CapsuleListener
{

    protected $data;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  CapsuleEvent  $event
     * @return void
     */
    public function handle(CapsuleEvent $event)
    {
        foreach ($event->capsule as $k => $v){
            $event->data = [];
            if(!is_array($v)){
                if($k == 'original_launch' && !is_null($v)){
                    $data [$k] = Carbon::parse($v)->format('Y-m-d H:i:s');
                }else{
                    $data [$k] = $v;
                }
                Log::info($k .' => '.$v);
            }else{
                $event->mission = [];
                foreach ($v as $v2){
                    $event->mission = $v;
                    foreach ($v2 as $k3 => $v3){
                        Log::info($k3 .' => '.$v3);
                    }
                }
            }
        }
        $c = Capsule::create($data);
        foreach ($event->mission as $k3 => $v3){
            $v3['capsule_id'] = $c->id;
            Log::info($c->id. ' : ' .$v3['name'] .' => '.$v3['flight']);
            Mission::create($v3);
        }

    }
}
