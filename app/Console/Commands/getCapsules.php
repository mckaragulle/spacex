<?php

namespace App\Console\Commands;

use App\Events\CapsuleEvent;
use App\Jobs\CapsuleStartJob;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Notification;

class getCapsules extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:capsules';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get all capsules data from https://api.spacexdata.com/v3/capsules';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $url = 'https://api.spacexdata.com/v3/capsules';
        $datas = json_decode(file_get_contents($url), true);
        if(is_array($datas) && count($datas) > 0){
            foreach ($datas as $data) {
                event(new \App\Events\CapsuleEvent($data));
            }
        }

        return 0;
    }
}
